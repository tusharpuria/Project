import React, { Component } from 'react';
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom';
import ContactListPage from './pages/contact-list-page';
import ContactFormPage from './pages/contact-form-page';
import GroupFormPage from './pages/group-form-page';
import Login from './Login/login';
import EditContactForm from './components/edit-contact-form';
import EditGroupForm from './components/edit-groups-form';
import ContactStore from './store/ContactStore';
import App from './App';
import { observer, inject } from 'mobx-react';
import Button from '@material-ui/core/Button';
import MainContacts from './ui/Main-contacts-component';
import GroupItems from './ui/Group-list';
import CssBaseline from '@material-ui/core/CssBaseline';


@inject('ContactStore')
@observer
class Routes extends Component {
  render() {
    const { ContactStore } = this.props;
    const authorized = localStorage.getItem('isAuthenticated');
    console.log("auth", authorized);
    return <BrowserRouter>
      {/* <CssBaseline/> */}
      <Switch>
        {/* <BrowserRouter> */}
        <Route exact path='/home' component={Login} />
        <Route exact path='/contacts' component={ContactListPage} />
        {/* <Route exact path="/home" render={() =>  {
          return authorized==='false' ? <Login /> :<Redirect  to = "/contacts"/> ; } } />
          <Route exact path="/contacts" render={() =>  {
          return authorized==='false' ? <Login /> : <ContactListPage /> ; } } /> */}
        <Route exact path="/contacts/new/:groupId" render={(props) => <ContactFormPage {...props} />} />
        <Route exact path="/contacts/edit/:_id" render={(props) => <EditContactForm {...props} />} />
        <Route exact path="/groups/new" render={() => <GroupFormPage />} />
        <Route exact path="/groups/edit/:_id" render={(props) => <EditGroupForm {...props} />} />
        <Route path="/" component={App} />
      </Switch>
    </BrowserRouter>
  }
}
export default Routes;