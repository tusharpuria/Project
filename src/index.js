import React from "react";
import { render } from "react-dom";
import { BrowserRouter } from 'react-router-dom';
import { Provider } from "mobx-react";
import ContactStore from './store/ContactStore';
import GroupStore from './store/GroupStore';
import AuthStore from './store/authStore';
import Routes from './routes'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const stores={
  ContactStore,
  GroupStore, 
  AuthStore
}
const theme = createMuiTheme();
render(
  <Provider {...stores}>
    <MuiThemeProvider theme={theme}>
      <Routes/>
    </MuiThemeProvider>  
  </Provider>,
  document.getElementById("root")
);

