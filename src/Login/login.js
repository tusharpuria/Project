import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';
import teal from '@material-ui/core/colors/teal';

const styles = {
    root: {
        flexGrow: 1,
    },
    loginButton: {
        textAlign: 'center'
    },
    description: {
        height: `${50}%`,
        textAlign: 'center'
    },
    content: {
        marginTop: `${20}px`,
        textAlign: 'center',
    }
}
@inject('ContactStore')
@withStyles(styles)
@observer
class Login extends Component {
    LogIn() {
        localStorage.setItem('isAuthenticated', 'true');
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <AppBar position="static" color="primary">
                    <Toolbar style = {{justifyContent: "center"}}>
                        <Typography variant="title" color="inherit" >
                            ShareNow
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Grid container direction="column" alignItems="center" spacing={24}>
                    <Grid item xs sm className={classes.content}>
                        <Typography variant="display1" align="center" gutterBottom>
                            Manange your Organisation's Contacts
                        </Typography>
                        <Typography variant="subheading" gutterBottom align="center">
                            A simple, easy and one place Web Application to Create, Update and Delete your Shared contacts in G-Suite.
                        </Typography>
                        <Typography variant="subheading" gutterBottom align="center">
                            Get Started, Login now.
                        </Typography>
                    </Grid>
                    <Grid item xs sm className={classes.loginButton}>
                        <Button onClick={this.LogIn.bind(this)} href="http://localhost:3030/auth/google" variant="raised" color="primary">
                            Log In with Google
                        </Button>
                    </Grid>
                </Grid>
            </div>
        )
    }
}
export default Login;