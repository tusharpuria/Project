import React, { Component} from 'react';
import GroupForm from '../components/group-form';

class GroupFormPage extends Component {
  render() {
    return (
      <div>
        <GroupForm />
      </div>
    )
  }
}
export default GroupFormPage;