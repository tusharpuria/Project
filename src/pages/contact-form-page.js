import React, { Component} from 'react';
import ContactForm from '../components/contact-form';

class ContactFormPage extends Component {
  constructor(props){
    super(props)
  }
  render() {
    return (
      <div>
        <ContactForm {...this.props}/>
      </div>
    )
  }
}
export default ContactFormPage;