import React, { Component} from 'react';
import {NavLink} from 'react-router-dom';
import {observer} from 'mobx-react';
import ContactList from '../components/contact-list';
import GroupList from '../components/groups-list';
import Paper from '@material-ui/core/Paper';

@observer
class ContactListPage extends Component {
  LogOut(){
    document.cookie = 'feathers-jwt=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    localStorage.setItem('isAuthenticated', 'false');
  }
  render() {
    console.log('contact list page')
    return <div>
        <button onClick ={this.LogOut.bind(this)}>
          <a href = "http://localhost:8080/">Log out</a>
        </button><br/>
          <NavLink exact to={`/contacts/new/1`}>
            Add Contact
          </NavLink>
        <Paper>
        <h1>List of Contacts</h1>
        <ContactList />
        </Paper>
        <NavLink exact to="/groups/new">
            Add group
          </NavLink>
        <Paper>
        <h1>List of Groups</h1>
        <GroupList/>
        </Paper>
      </div>
  }
}
export default ContactListPage;