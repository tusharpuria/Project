import {observable, computed, action, toJS} from 'mobx';
import ServerCall from './api-call';

class GroupStore {    
    @observable groups =[];
    @observable group; 
    @observable selected;
        
    @action async fetchAllGroups(){
        this.groups = await ServerCall.ApiCall('GET', 'http://localhost:3030/groups/');
        return this.groups;
    }    
    @action async addGroup(data){
        const header ={
            key : 'Content-Type',
            value : 'application/json'
        }
        const res = await ServerCall.ApiCall('POST','http://localhost:3030/groups/', header, JSON.stringify(data))
    }
    @action async deleteGroup(groupId, groupTag){
        const res = await ServerCall.ApiCall('DELETE', `http://localhost:3030/groups/${groupId}?groupTag=${groupTag}`)
        this.fetchAllGroups();
    }
    @action async updateGroup(data){    
        const header =[{
            key : 'Accept',
            value : 'application/json'
        },{
            key : 'Content-Type',
            value : 'application/json'
        }]
        const res = await ServerCall.ApiCall('PATCH',`http://localhost:3030/groups/${data.groupId}`, header, JSON.stringify(data))
        this.fetchAllGroups();        
    }
    @action async fetchGroupFromId(id){
        this.group = this.groups.filter(c => c.groupId === id)[0];
        
    }
}

export default GroupStore = new GroupStore();