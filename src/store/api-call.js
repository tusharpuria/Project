import {observable, computed, action, toJS} from 'mobx';
import AuthStore from './authStore';

class ServerCall {
    @observable auth = AuthStore.getCookie("feathers-jwt"); 

    @action ApiCall = async (method, url, header, body) => {
        const headers = new Headers();
        headers.append('Authorization', this.auth);
        if (header) {
            if (header.length && header.length > 1) {
                header.map(head => headers.append(head.key, head.value));
            }
            else {
                headers.append(header.key, header.value)
            }
        }
        const options = {
            method,
            headers,
        };
        if (body) {
            options.body = body
        }
        const request = new Request(url, options);
        const response = await fetch(request);
        if (!response.ok) {
            AuthStore.ErrorMessage(response);
        }
        const json = await response.json();
        return json.data;
    }
}

export default ServerCall = new ServerCall();