import { observable, computed, action, toJS } from 'mobx';

class AuthStore {
    @observable message;

    getCookie = (name) => {
        var regexp = new RegExp("(?:^" + name + "|;\s*" + name + ")=(.*?)(?:;|$)", "g");
        var result = regexp.exec(document.cookie);
        return (result === null) ? null : result[1];
    }

    ErrorMessage(error){
        switch(error.status){
            case 401:{
                localStorage.setItem('isAuthenticated', 'false');
                this.message = 'You need to login for this, Please login'
                break;
            }
            case 403:{
                this.message = "You don't have access for this"
                break;
            }
            case 400:{
                this.message = 'Bad request'
                break;
            }
            case 408:{
                this.message = 'cannot fetch data, try again'
                break;
            }
            default:{
                this.message = 'Something went wrong'
                break;
            }
        }
    }
}

export default AuthStore = new AuthStore();