import { observable, computed, action, toJS } from 'mobx';
import AuthStore from './authStore';
import ServerCall from './api-call';

class ContactStore {
    @observable contacts = [];
    @observable contact;
    @observable selected = true;
    
    @action async fetchAllContacts() {
        this.contacts = await ServerCall.ApiCall('GET', 'http://localhost:3030/contact/');
        return this.contacts;
    }
    @action async addContact(data, groupId) {
        let header;
        data.append('groupId', groupId);
        const imgRes = await ServerCall.ApiCall('POST', 'http://localhost:3030/contact/', header, data)
    }
    @action async deleteContact(contactId, eTag) {
        const res = await ServerCall.ApiCall('DELETE', `http://localhost:3030/contact/${contactId}?eTag=${eTag}`)
        await this.fetchAllContacts();
    }
    @action async updateContact(data, contactId, groupId = null) {
        if (groupId == null) {
            data.append('groupId', 'null');
        }
        else {
            data.append('groupId', groupId);
        }
        const header = {
            key: 'Accept',
            value: 'application/json'
        }
        const res = await ServerCall.ApiCall('PATCH', `http://localhost:3030/contact/${contactId}`,
            header, data)
    }
    @action async fetchContactFromId(id) {
        this.contact = this.contacts.filter(c => c.contactId === id)[0];
    }
    @action async addContactToGroup(groupId) {
        let JsonObject = this.contact;
        JsonObject["groupId"] = groupId;
        const header = [{
            key: 'Accept',
            value: 'application/json'
        }, {
            key: 'Content-Type',
            value: 'application/json'
        }]
        const res = await ServerCall.ApiCall('PATCH', `http://localhost:3030/contact/${this.contact.contactId}`
            , header, JSON.stringify(JsonObject))
    }
}

export default ContactStore = new ContactStore();