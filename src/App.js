import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom';
import ContactListPage from './pages/contact-list-page';
import ContactFormPage from './pages/contact-form-page';
import Login from './Login/login';
import EditContactForm from './components/edit-contact-form';
import ContactStore from './store/ContactStore';

import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { inject, observer } from 'mobx-react';
import MainContacts from './ui/Main-contacts-component';
import GroupItems from './ui/Group-list';
import ContactDetails from './ui/Contact-details';
import EditContact from './ui/Contact-Edit';
import GroupContacts from './ui/Group-contacts';

@inject('ContactStore')
@withRouter
@observer
class App extends Component {
  render() {
    return <div>
      <Route exact path="/" render={() => <Redirect to="/home" />} />
      <Route exact path="/dummy/contacts" render={(props) => <MainContacts {...props} />} />
      <Route exact path="/dummy/contact/details/:id" render={(props) => <ContactDetails {...props} />} />
      <Route exact path="/dummy/contact/edit/:id" render={(props) => <EditContact {...props} />} />
      <Route exact path="/dummy/groups" render={(props) => <GroupItems {...props} />} />
      <Route exact path="/dummy/group/:id" render={(props) => <GroupContacts {...props} />} />
      <Route exact path="/dummy/contacts/new/:groupId" render={(props) => <ContactFormPage {...props} />} />
      <Route exact path="/dummy/groups/new" render={() => <GroupFormPage />} />
      <Route exact path="/dummy/groups/edit/:id" render={(props) => <EditGroupForm {...props} />} />
    </div>
  }
}
export default App;