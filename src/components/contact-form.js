import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { toJS, observable, action } from 'mobx';
import { Redirect } from 'react-router-dom';

@inject("ContactStore")
@observer
class ContactForm extends Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  options = ['mobile', 'work', 'home', 'fax', 'other'];
  @observable body = new FormData();
  @observable data = {
    firstName: '',
    email:'',
    lastName: '',
    phone: '',
    phoneType: 'mobile',
    image: {},
    streetAddress: '',
    city: '',
    street: '',
    region: '',
    postcode: '',
    country: '',    
  }
  @action handleChange = name => event => {
    this.data[name] = `${event.target.value}`
  }
  @action handleFile = (file) => event => {
    this.data[file] = event.target.files[0];
  }
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input type="file" name="image" onChange={this.handleFile("image")} />
          <input type="text" name="firstName" onChange={this.handleChange('firstName')} placeholder="First Name" />
          <input type="text" name="lastName" onChange={this.handleChange('lastName')} placeholder="Last Name" />
          <input type="text" name="email" onChange={this.handleChange('email')} placeholder="Email" />
          <select onChange={this.handleChange('phoneType')} name="phoneType">
            {this.options.map(option => <option key={option}>{option}</option>)}
          </select>
          <input type="text" name="phone" onChange={this.handleChange('phone')} placeholder="Phone number" />
          <input type="text" name="city" onChange={this.handleChange('city')} placeholder="city" />
          <input type="text" name="street" onChange={this.handleChange('street')} placeholder="street" />
          <input type="text" name="region" onChange={this.handleChange('region')} placeholder="region" />
          <input type="text" name="postcode" onChange={this.handleChange('postcode')} placeholder="post code" />
          <input type="text" name="country" onChange={this.handleChange('country')} placeholder="country" />

          <button type="submit">Add Contact</button>
        </form>
      </div>
    )
  }
  handleSubmit(event) {
    event.preventDefault();
    const d = Object.keys(this.data).map(key => {
      if (key == "image") {
        this.body.append(`${key}`, this.data[key])
      } else {
        this.body.append(`${key}`, `${this.data[key]}`)
      }
    })
    //console.log("id---", this.props.match.params.groupId);
    this.props.ContactStore.addContact(this.body, this.props.match.params.groupId);
    //window.location = "/contacts";
  }

}
export default ContactForm;
