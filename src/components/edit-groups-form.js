import React, { Component } from 'react';
import {observer, inject} from 'mobx-react'; 
import {toJS, observable, action} from 'mobx';
import {Redirect} from 'react-router-dom';

@inject("GroupStore")
@observer
class EditGroupForm extends Component {
  constructor(props){
    super(props)
    this.getGroups(this.props.match.params._id)
  }
  async getGroups(id){
    await this.props.GroupStore.fetchGroupFromId(id)
  }
  @observable formData = {
    name: this.props.GroupStore.group.name,
    groupTag: this.props.GroupStore.group.groupTag,
    groupId: this.props.GroupStore.group.groupId
  }
  @action handleChange = name => event => {
    this.formData[name] = `${event.target.value}`
  }
  render() {
    const {GroupStore} = this.props;
    const groupData = toJS(GroupStore.group);
    return (
      <div>
        {groupData ? 
        <form onSubmit ={ this.handleSubmit(groupData.groupTag)}>
          <input type="text" name= "name" onChange={this.handleChange('name')} defaultValue = {groupData.name}/>
          {" "}
          <button type = "submit" >Save</button>
        </form> : <div></div> }        
      </div>       
    )
  }
  handleSubmit = (groupTag) => (event) => { 
    event.preventDefault();
    this.props.GroupStore.updateGroup(this.formData); 
    window.location = "/contacts";  
  }  
}
export default EditGroupForm;