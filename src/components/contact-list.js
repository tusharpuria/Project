import React, { Component } from 'react';
import { observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import EditContactForm from './edit-contact-form';
import { Link } from 'react-router-dom';

@inject('ContactStore', 'GroupStore', 'AuthStore')
@observer
export default class ContactList extends Component {
  componentDidMount() {
    if(this.props.AuthStore.getCookie('feathers-jwt')){
      localStorage.setItem('isAuthenticated', 'true');
      this.props.ContactStore.fetchAllContacts();
    this.props.GroupStore.fetchAllGroups();
    }
  }
  handleClick(contactId, eTag) {
    this.props.ContactStore.deleteContact(contactId, eTag);
  }
  addToGroup(contactId, groupId) {
    this.props.ContactStore.fetchContactFromId(contactId);
    this.props.ContactStore.addContactToGroup(groupId);
  }
  render() {
    let key = 0;
    console.log("t--", this.props.ContactStore.contacts.length && this.props.ContactStore.contacts.length > 1);
    return (
      <div>
        {this.props.ContactStore.contacts.length && this.props.ContactStore.contacts.length > 1 ? 
        this.props.ContactStore.contacts.map(contact => {
          return (<div key={key++} >
            {contact.photoTag ? <img src={`${contact.photoUrl}`} alt="photo" />
              : ""}
            <li>Name: {contact.fullName}</li>
            <li>Phone number: {contact.phone}</li>
            <button onClick={() => this.handleClick(`${contact.contactId}`, `${contact.eTag}`)
            }>Delete</button>{" "}
            <button><Link to={`/contacts/edit/${contact.contactId}`}>Edit</Link></button>
            Add to group
              {this.props.GroupStore.groups.map(group => <li key={group.groupId} onClick={() =>
              this.addToGroup(`${contact.contactId}`, `${group.groupId}`)}><a href="#">{group.name}</a></li>)}
          </div>
          )
        }
        ) : ""}
      </div>
    );
  }
};