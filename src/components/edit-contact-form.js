import React, { Component } from 'react';
import {observer, inject} from 'mobx-react'; 
import {toJS, observable, action} from 'mobx';
import {Redirect} from 'react-router-dom';

@inject("ContactStore")
@observer
class EditContactForm extends Component {
  constructor(props){
    super(props)
    this.getContact(this.props.match.params._id)
  }
  async getContact(id){
    await this.props.ContactStore.fetchContactFromId(id)
  }
  @observable body = new FormData();
  @observable formData = {
    firstName: this.props.ContactStore.contact.firstName,
    lastName: this.props.ContactStore.contact.lastName,
    phone: this.props.ContactStore.contact.phone,
    image: {}
  }

  @action handleChange = name => event => {
    this.formData[name] = `${event.target.value}`
  }
  @action handleFile = (file) => event => {
    this.formData[file] = event.target.files[0];
  } 
  render() {
    const {ContactStore} = this.props;
    const data = toJS(ContactStore.contact);
    return (
      <div>        
        {data ?
        <div> 
        <img src={`${data.photoUrl}`} alt="photo"/>
        <form onSubmit ={this.handleSubmit(data.photoTag, data.eTag)}>          
          <input type="text" name= "firstName" onChange={this.handleChange('firstName')} defaultValue = {data.firstName}/>
          <input type="text" name= "lastName" onChange={this.handleChange('lastName')} defaultValue = {data.lastName}/>
          <input type="text" name= "phone" onChange={this.handleChange('phone')} defaultValue= {data.phone}/>
          <input type="file" name="image" onChange={this.handleFile("image")}/>
          {" "}
          <button type = "submit" >Save</button></form>
          </div> : <div></div> }        
      </div>       
    )
  }
  @action handleSubmit = (photoTag, eTag) => event => { 
    event.preventDefault();
    const d= Object.keys(this.formData).map(key => {
      if(key == "image"){
        this.body.append(`${key}`, this.formData[key])
      }else{
      this.body.append(`${key}`, `${this.formData[key]}`)}
    })
    this.body.append('photoTag', photoTag);
    this.body.append('eTag', eTag);
    this.props.ContactStore.updateContact(this.body, this.props.match.params._id); 
    window.location = "/contacts";  
  }  
}
export default EditContactForm;