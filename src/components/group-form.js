import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import {toJS, observable, action} from 'mobx';
import {Redirect} from 'react-router-dom';

@inject("GroupStore")
@observer
class GroupForm extends Component { 
  @observable formData = {
    name: ''
  }
  @action handleChange = name => event => {
    this.formData[name] = `${event.target.value}`
  } 
  render() {
    return (
      <div>
        <form onSubmit = {this.handleSubmit.bind(this)}>
          <input type="text" name= "name" onChange={this.handleChange('name')} placeholder ="Name" />
          <button type = "submit">Add Group</button>
        </form>
      </div>
    )
  }
  handleSubmit(event){    
    event.preventDefault();
    this.props.GroupStore.addGroup(this.formData);
    window.location = "/contacts";   
  }  
}
export default GroupForm;
