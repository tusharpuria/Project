import React, { Component } from 'react';
import { observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';

@inject('GroupStore')
@observer
export default class GroupsList extends Component {
  componentDidMount() {
    if(localStorage.getItem('isAuthenticated')=== true){
      return this.props.GroupStore.fetchAllGroups();
    }
  }
  handleClick(groupId, groupTag) {
    this.props.GroupStore.deleteGroup(groupId, groupTag);
  }
  render() {
    let key = 0;
    return (
      <div>
        {this.props.GroupStore.groups.length && this.props.GroupStore.groups.length > 1 ? this.props.GroupStore.groups.map(group => {
          return (<div key={key++} >
            <li>Name: {group.name}</li>
            <button onClick={() => this.handleClick(`${group.groupId}`, `${group.groupTag}`)
            }>Delete</button>{" "}
            <button><Link to={`/groups/edit/${group.groupId}`}>Edit</Link></button>
            <button><Link to={`/contacts/new/${group.groupId}`}>Add contact</Link></button>
          </div>
          )
        }
        ) : ""}
      </div>
    );
  }
};

