import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {
	IconButton, Icon, Button, TextField,
	Typography, Grid, Checkbox, Paper, DialogContent, Dialog,
	DialogTitle, DialogContentText, DialogActions
} from '@material-ui/core';

const styles = theme => ({
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: 150,
	},
	dialogButton: {
		marginTop: 0,
		paddingTop: 0,
		justifyContent: "space-evenly"
	},
	dialogContent:{
		textAlign: "center",
		paddingTop:5
	}
})
@withStyles(styles)
@observer
class AddGroup extends Component {
	@action handleClose() {
		this.props.close();
	}
	render() {
		const { open, classes } = this.props;
		return (
			<Dialog open={open} onClose={() => this.handleClose()}>
				<DialogTitle style ={{paddingBottom: 5}}>
					Add Group
				</DialogTitle>
				<DialogContent className={classes.dialogContent}>
					<TextField
						label="Name"
						className={classes.textField}
						margin="normal"
					/>
				</DialogContent>
				<DialogActions className={classes.dialogButton}>
					<Button onClick={() => this.handleClose()} color="primary" variant="raised">
						Create
          			</Button>
					<Button onClick={() => this.handleClose()} color="primary" variant="raised" autoFocus>
						Discard
          			</Button>
				</DialogActions>
			</Dialog>
		);
	}
}
export default AddGroup;
