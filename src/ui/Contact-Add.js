import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {
	IconButton, Icon, Button, TextField,
	Typography, Grid, Checkbox, Paper, DialogContent, Dialog,
	DialogTitle, DialogContentText, DialogActions
} from '@material-ui/core';

const styles = theme => ({
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		maxWidth: 150,
	},
	dialogButton: {
		marginTop: 0,
		paddingTop: 0,
		justifyContent: "space-evenly"
	},
	dialogContent: {
		textAlign: "center",
		paddingTop: 5
	}
})
@withStyles(styles)
@observer
class AddContact extends Component {
	@action handleClose() {
		this.props.onCloseAdd();
	}
	render() {
		const { openAdd, onCloseAdd, classes } = this.props;
		return (
			<Dialog open={openAdd} onClose={() => this.handleClose()}>
				<DialogTitle style={{ paddingBottom: 5, textAlign: "center" }}>
					Add Contact
				</DialogTitle>
				<DialogContent className={classes.dialogContent}>
					<Grid container >
						<Grid item container direction="row" alignItems="flex-end">
							<Grid item>
								<Icon>person</Icon>
							</Grid>
							<Grid item >
								<TextField
									label="First Name"
									className={classes.textField}
								/>
							</Grid>
							<Grid item>
								<TextField
									label="last Name"
									className={classes.textField}
								/>
							</Grid>
						</Grid>
						<Grid item container direction="row" alignItems="flex-end">
							<Grid item>
								<Icon>phone</Icon>
							</Grid>
							<Grid item xs ={8} sm = {8}>
								<TextField
									label="Phone number"
									margin="normal"
									fullWidth = {true}
								/>
							</Grid>
						</Grid>
						<Grid item container direction="row" alignItems="flex-end">
							<Grid item>
								<Icon>mail</Icon>
							</Grid>
							<Grid item xs ={8} sm = {8}>
								<TextField
									label="Email"
									margin="normal"
									fullWidth = {true}
								/>
							</Grid>
						</Grid>
					</Grid>
				</DialogContent>
				<DialogActions className={classes.dialogButton}>
					<Button onClick={() => this.handleClose()} color="primary" variant="raised">
						Create
          			</Button>
					<Button onClick={() => this.handleClose()} color="primary" variant="raised" autoFocus>
						Discard
          			</Button>
				</DialogActions>
			</Dialog>
		);
	}
}
export default AddContact;
