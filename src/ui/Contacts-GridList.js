import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { GridList, GridListTile, GridListTileBar, ListSubheader, IconButton, Icon } from '@material-ui/core';

const styles = theme => ({
	root: {
		marginTop: 5,
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		backgroundColor: theme.palette.background.paper,
	},
	gridList: {
		width: 500,
		height: 450,
	},
	icon: {
		color: 'rgba(255, 255, 255, 0.54)',
	},
});
@withStyles(styles)
class ContactsGridList extends Component {
	render() {
		const { classes } = this.props;
		const tileData = [{
			img: 0,
			name: "contact1",
			phone: "12345"
		},
		{
			img: 1,
			name: "contact2",
			phone: "12345"
		},
		{
			img: 2,
			name: "contact3",
			phone: "12345"
		}
		]
		return (
			<div className={classes.root}>
				<GridList cellHeight={180} className={classes.gridList}>
					{tileData.map(tile => (
						<GridListTile key={tile.img}>
							<Icon>image</Icon>
							<GridListTileBar
								title={tile.name}
								subtitle={<span> {tile.phone}</span>}
								actionIcon={
									<IconButton className={classes.icon}>
										<Icon>info </Icon>
									</IconButton>
								}
							/>
						</GridListTile>
					))}
				</GridList>
			</div>
		);
	}
}

export default ContactsGridList;