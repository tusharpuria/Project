import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link, Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {
	CssBaseline, IconButton, Avatar, Icon, Button,
	ListItemSecondaryAction, ListItemText, ListItem, List,
	Typography, Grid, Checkbox, Paper, Dialog, DialogContent,
	DialogContentText, DialogTitle, DialogActions
} from '@material-ui/core';
import ContactDetails from './Contact-details';
import AddContact from './Contact-Add';

const styles = theme => ({
	root: {
		width: '100%',
		minWidth: 250,
		backgroundColor: theme.palette.background.paper,
	},
	button: {
		margin: theme.spacing.unit * 0.5,
	},
	link: {
		textDecoration: 'none'
	},
	floatButton:
		{
			margin: 0,
			top: 'auto',
			right: 20,
			bottom: 20,
			left: 'auto',
			position: 'fixed',
		}
});

@withStyles(styles)
@observer
class ContactItems extends Component {
	@observable addValue = false;
	@action handleAddOpen() {
		return this.addValue = true;
	}
	@action handleAddClose() {
		return this.addValue = false;
	}
	render() {
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				<Paper>
					<Button onClick={() => this.handleAddOpen()} variant="fab" color="primary" aria-label="add" className={classes.floatButton} >
						<Icon>add</Icon>
					</Button>
					<List >
						<Link className={classes.link} to="/dummy/contact/details/1">
							<ListItem button >
								<Avatar>
									<Icon>image</Icon>
								</Avatar>
								<ListItemText primary="Contacts1" secondary="123456" />
								<Icon></Icon>

							</ListItem>
						</Link>
						<ListItem button>
							<Avatar>
								<Icon>image</Icon>
							</Avatar>
							<ListItemText primary="Contacts2" secondary="789456" />
						</ListItem>
						<ListItem button>
							<Avatar>
								<Icon>image</Icon>
							</Avatar>
							<ListItemText primary="Contacts3" secondary="456789" />
						</ListItem>
					</List>
				</Paper>
				<AddContact openAdd={this.addValue} onCloseAdd={() => this.handleAddClose()} />
			</div>
		);
	}
}

export default ContactItems;