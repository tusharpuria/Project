import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link, Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {
  CssBaseline, IconButton, Avatar, Icon, Button,
  ListItemSecondaryAction, ListItemText, ListItem, List,
  Typography, Grid, Checkbox, Paper, DialogContent, Dialog,
  DialogTitle, DialogContentText, DialogActions, Divider
} from '@material-ui/core';
import EditContact from './Contact-Edit';
import TitleBar from './App-bar';

const styles = theme => ({
  title: {
    display: 'flex',
    alignItems: 'center'
  },
  back: {
    paddingLeft: 0,
    marginRight: 10
  },
  link: {
    textDecoration: 'none'
  }, 
  spacing:{
    paddingRight:8,
    marginRight:8
  }
})
@withStyles(styles)
@observer
class ContactDetails extends Component {
  @observable back = false;
  @observable value = false;
  @action handleEdit() {
    this.value = true
  }
  @action handleBack() {
    this.value = false;
  }
  @action goBack() {
    return <Redirect to="/contacts" />
  }
  render() {
    const { classes, close } = this.props;
    console.log("prop--", this.props.location)
    return (
      <div>
        {this.value ? <Redirect to="/dummy/contact/edit/1" /> :
        <div>
          <TitleBar name = 'Contacts'/>
          <Grid>
            <Grid item className={classes.title}>
              <Link className={classes.link} to="/dummy/contacts">
                <IconButton className={classes.back} >
                  <Icon>arrow_back</Icon>
                </IconButton>
              </Link>
              <Typography variant="subheading" style={{ flex: 0.75 }}>
                Contacts1
						  </Typography>
              <Link className={classes.link} to="/dummy/contact/edit/1">
                <IconButton >
                  <Icon >create</Icon>
                </IconButton>
              </Link>
              <IconButton >
                <Icon >delete</Icon>
              </IconButton>
            </Grid>
            <Divider />
            <Grid item style ={{marginTop:15}}>
              <List >
                <ListItem>
                  <Icon className={classes.spacing}>image</Icon>
                  <ListItemText primary="Contacts 1"  />
                </ListItem>
                <ListItem >
                  <Icon className={classes.spacing}>phone</Icon>
                  <Grid container spacing={8}>
                    <Grid item sm={2} md={1}>
                      <ListItemText primary="95123" secondary="work" />
                    </Grid>
                    <Grid item sm={2} md={1}>
                      <ListItemText primary="95123" secondary="home" />
                    </Grid>
                  </Grid>
                </ListItem>
                <ListItem >
                  <Icon className={classes.spacing}>email</Icon>
                  <ListItemText primary="Contact@c.com" />
                </ListItem>
                <ListItem >
                  <Icon className={classes.spacing}>location_on</Icon>
                  <ListItemText primary="1600 Amphitheatre Pkwy Mountain View, CA 94043 United States" />
                </ListItem>
              </List>
            </Grid>
          </Grid>
          </div>}
      </div>
    );
  }
}
export default ContactDetails;


// @observable openEdit = false;
// @action handleClose() {
// 	this.props.onClose();
// }
// @action handleEditOpen() {
// 	this.openEdit = true;
// }
// @action handleEditClose() {
// 	this.openEdit = false;
// }
// render() {
// 	const { open, onClose, classes } = this.props;
// 	return (
// 		<Dialog open={open} onClose={() => this.handleClose()}>
// 			<DialogTitle>
// 				Contact Details
// 		</DialogTitle>
// 			<DialogContent>
// 				<List >
// 					<ListItem>
// 						<ListItemText primary="First Name:" />
// 						<Typography variant="title">
// 							qwerty
// 					</Typography>
// 					</ListItem>
// 					<ListItem>
// 						<ListItemText primary="Last Name:" />
// 						<Typography variant="title">
// 							ui
// 					</Typography>
// 					</ListItem>
// 					<ListItem>
// 						<ListItemText primary="Phone number:" />
// 						<Typography variant="title">
// 							1234
// 					</Typography>
// 					</ListItem>
// 				</List>
// 			</DialogContent>
// 			<DialogActions className={classes.dialogButton}>
// 				<Button onClick={() => this.handleEditOpen()} color="primary" variant="raised">
// 					Edit
// 		</Button>
// 				<Button onClick={() => this.handleClose()} color="primary" variant="raised" autoFocus>
// 					Delete
// 		</Button>
// 			</DialogActions>
// 			<EditContact openEdit={this.openEdit} onCloseEdit={() => this.handleEditClose()} />

// 		</Dialog>
// 	);
// }