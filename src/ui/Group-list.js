import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link, Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {
	CssBaseline, IconButton, Avatar, Icon, Button,
	ListItemSecondaryAction, ListItemText, ListItem, List,
	Typography, Grid, Checkbox, Paper
} from '@material-ui/core';
import AddGroup from './Group-Add';
import TitleBar from './App-bar';

const styles = theme => ({
	root: {
		marginTop: 5,
		width: '100%',
		minWidth: 250,
		backgroundColor: theme.palette.background.paper,
	},
	button: {
		margin: theme.spacing.unit * 0.5,
	},
	grid: {
		alignSelf: "center"
	},
	link:{
		textDecoration: "none"
	},
	floatButton:
		{
			margin: 0,
			top: 'auto',
			right: 20,
			bottom: 20,
			left: 'auto',
			position: 'fixed',
		}
});
@inject('GroupStore', 'ContactStore')
@withStyles(styles)
@observer
class GroupItems extends Component {
	@observable open = false;
	@action handleOpen = () => {
		this.open = true;
	}
	@action handleClose = () => {
		this.open = false;
	}
	render() {
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				<TitleBar name='Groups' />
				<Paper>
					<Button onClick={() => this.handleOpen()} variant="fab" color="primary" aria-label="add" className={classes.floatButton} >
						<Icon>add</Icon>
					</Button>
					<List >
						<Link className = {classes.link} to="/dummy/group/1">
							<ListItem button>
								<ListItemText primary="Group1" />
								<Typography>5 contacts</Typography>
							</ListItem>
						</Link>
						<ListItem button>
							<ListItemText primary="Group2" />
							<Typography>5 contacts</Typography>
						</ListItem>
						<ListItem button>
							<ListItemText primary="Group3" />
							<Typography>5 contacts</Typography>
						</ListItem>
					</List>
				</Paper>
				<AddGroup open={this.open} close={() => this.handleClose()} />
			</div>
		);
	}
}

export default GroupItems;