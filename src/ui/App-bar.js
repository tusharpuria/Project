import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link, Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {
    Tab, Tabs, Paper, Drawer, AppBar, Toolbar,
    List, ListItem, IconButton, Divider, ListItemIcon,
    ListItemText, MenuItem, Typography, TextField, Icon,
    Menu, ClickAwayListener, Button, Grow, MenuList, Popover,
    Input, Grid, CssBaseline
} from '@material-ui/core';
import { Manager, Target, Popper } from 'react-popper';

const styles = theme => ({
    body: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 24,
        paddingLeft: 24
    },
    icon: {
        paddingLeft: theme.spacing.unit * 0.5
    },
    link :{
        textDecoration: 'none'
    }
});

@withStyles(styles)
@observer
class TitleBar extends Component {
    @observable link = this.props.name === 'Groups' ? 'contacts': 'groups';
    @observable value = this.props.name;
    @observable anchorEl = null;
    @observable ConAnchor = null;
    @observable open = false;
    constructor(props) {
        super(props)
        this.handlePopoverOpen = this.handlePopoverOpen.bind(this);
    }
    @action handleChange = (event) => {
        this.value = event.target.firstChild.nodeValue;
        if(this.value === 'Contacts'){
            this.link = 'groups'
        }
        else{   
            this.link = 'contacts'         
        }
        this.handlePopoverClose()
    };

    @action handleClick = event => {
        this.anchorEl = event.currentTarget;
    };
    @action handleClose = () => {
        this.open = false;
        this.anchorEl = null;
    };
    @action handlePopoverOpen = event => {
        this.ConAnchor = event.currentTarget;
    };

    @action handlePopoverClose = () => {
        this.ConAnchor = null;
    };

    render() {
        const { classes, name } = this.props;
        return (<AppBar position="static">
            <Toolbar >
                <IconButton color='inherit'><Icon >portrait</Icon></IconButton>
                <div style={{ flex: 1 }}>
                    <Button color="inherit" onClick={this.handlePopoverOpen}> {name}
                        <Icon className={classes.icon}>arrow_drop_down</Icon>
                    </Button>
                </div>
                <Popover
                    open={Boolean(this.ConAnchor)}
                    anchorEl={this.ConAnchor}
                    onClose={this.handlePopoverClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                    
                >   
                <div onClick={this.handleChange}>
                <Link to ={`/dummy/${this.link}`} className ={classes.link}>
                    <MenuItem >{this.value === 'Contacts' ? 'Groups' : 'Contacts'}
                    </MenuItem>
                    </Link>
                </div>
                </Popover>
                <IconButton
                    aria-owns={this.anchorEl ? 'simple-menu' : null}
                    aria-haspopup="true"
                    onClick={this.handleClick}
                    color="inherit">
                    <Icon>more_vert</Icon>
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={this.anchorEl}
                    open={Boolean(this.anchorEl)}
                    onClose={() => this.handleClose()}
                >
                    <MenuItem onClick={() => this.handleClose()}>Logout</MenuItem>
                </Menu>
            </Toolbar>
        </AppBar>
        );
    }
}
export default TitleBar;