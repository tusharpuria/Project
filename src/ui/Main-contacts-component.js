import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link, Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {
  Tab, Tabs, Paper, Drawer, AppBar, Toolbar,
  List, ListItem, IconButton, Divider, ListItemIcon,
  ListItemText, MenuItem, Typography, TextField, Icon,
  Menu, ClickAwayListener, Button, Grow, MenuList, Popover,
  Input, Grid, CssBaseline
} from '@material-ui/core';
import ContactItems from './Contacts-list';
import ContactsGridList from './Contacts-GridList';
import GroupItems from './Group-list';
import { Manager, Target, Popper } from 'react-popper';
import TitleBar from './App-bar';

const styles = theme => ({
  body: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 24,
    paddingLeft: 24
  },
  icon: {
    paddingLeft: theme.spacing.unit * 0.5
  },
  search:{

  }
});
@inject('ContactStore', 'GroupStore')
@withStyles(styles)
@observer
class MainContacts extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div >
        <TitleBar name = 'Contacts'/>
        <Paper className={classes.body}>
          <Grid container alignItems="center">
            <Icon>search</Icon>
            <Input
              disableUnderline={true}
              placeholder="Search"
              id="search"
              type="search"
              style = {{flex:1}}
            />
          </Grid>
        </Paper>
         <ContactItems />

      </div>
    );
  }
}
export default MainContacts;

// <AppBar position="static">
// <Toolbar >
//   <IconButton color = 'inherit'><Icon >portrait</Icon></IconButton>
//   <div style ={{flex:1}}>
//   <Button color="inherit" onClick={this.handlePopoverOpen}> {this.value}
//     <Icon className = {classes.icon}>arrow_drop_down</Icon>
//   </Button>
//   </div>
//   <Popover
//     open={Boolean(this.ConAnchor)}
//     anchorEl={this.ConAnchor}
//     onClose={this.handlePopoverClose}
//     anchorOrigin={{
//       vertical: 'bottom',
//       horizontal: 'center',
//     }}
//     transformOrigin={{
//       vertical: 'top',
//       horizontal: 'center',
//     }}
//   >
//     <MenuItem onClick={this.handleChange}>{this.value === 'Contacts' ? 'Groups': 'Contacts'}</MenuItem>
//   </Popover>
//   <IconButton
//     aria-owns={this.anchorEl ? 'simple-menu' : null}
//     aria-haspopup="true"
//     onClick={this.handleClick}
//     color="inherit">
//     <Icon>more_vert</Icon>
//   </IconButton>
//   <Menu
//     id="simple-menu"
//     anchorEl={this.anchorEl}
//     open={Boolean(this.anchorEl)}
//     onClose={() => this.handleClose()}
//   >
//     <MenuItem onClick={() => this.handleClose()}>Logout</MenuItem>
//   </Menu>
// </Toolbar>
// </AppBar>