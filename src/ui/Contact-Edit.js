import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {
   IconButton, Icon, Button, TextField,
   Typography, Grid, Checkbox, Paper, DialogContent, Dialog,
   DialogTitle, DialogContentText, DialogActions, Divider, List,
   ListItem, ListItemText, Select, MenuItem, InputLabel
} from '@material-ui/core';
import TitleBar from './App-bar';

const styles = theme => ({
   title: {
      display: 'flex',
      alignItems: 'center'
   },
   back: {
      paddingLeft: 0,
      paddingRight: 20,
      marginRight:20,
      textDecoration: 'none'
   },
   textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 200,
   },
   shortTextField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 90,
   },
   paper: {
      width: '98%',
      minWidth: 200

   },
   grid: {
      marginRight: theme.spacing.unit,
      padding: theme.spacing.unit
   },
   flex: {
      flexDirection: "row"
   },
   fields: {
      alignItems: "flex-end",
      margin: 5,
      padding: 8
   },
   spacing: {
      margin: 3,
      padding: 3
   }
})
@withStyles(styles)
@observer
class EditContact extends Component {
   @observable phoneType = 'Mobile';
   @action handleChange = (event) => {
      this.phoneType = event.target.value;
   }
   render() {
      const { openEdit, onCloseEdit, classes } = this.props;
      return (
         <div>
            <TitleBar name= 'Contacts'/>
         <Grid>
            <Grid container item xs sm md className={classes.title} spacing={24}>
               <Grid item >
               <Link className={classes.back} to ='/dummy/contact/details/1'>
                  <IconButton >
                     <Icon >arrow_back</Icon>
                  </IconButton>
               </Link>
               </Grid>
               <Grid item style ={{flex:0.5}}>
                  <Typography variant="subheading" >
                     Edit
					</Typography>
               </Grid>
               <Grid item style ={{flex:0.5}}>
                  <IconButton >
                     <Icon >save</Icon>
                  </IconButton>
               </Grid>
            </Grid>
            <Divider />
            <Grid container item xs sm md >
                  <form  >
                     <Grid container>
                        <Grid container item className={classes.fields}>
                           <Grid item className={classes.spacing}>
                              <Icon>person</Icon>
                           </Grid>
                           <Grid item className={classes.spacing} >
                              <TextField
                                 label="First Name"
                                 className={classes.shortTextField}
                              />
                           </Grid>
                           <Grid item className={classes.spacing}>
                              <TextField
                                 label="Last Name"
                                 className={classes.shortTextField}
                              />
                           </Grid>
                        </Grid>
                        <Grid container direction="row" className={classes.fields}>
                           <Grid item className={classes.spacing}>
                              <Icon>phone</Icon>
                           </Grid>
                           <Grid item style={{ alignSelf: "flex-end" }} className={classes.grid}>
                              <TextField
                                 select
                                 value={this.phoneType}
                                 onChange={(event) => this.handleChange(event)}
                              >
                                 <MenuItem value="Mobile">Mobile</MenuItem>
                                 <MenuItem value="Work">Work</MenuItem>
                                 <MenuItem value="Home">Home</MenuItem>
                                 <MenuItem value="Other">Other</MenuItem>
                              </TextField>
                           </Grid>
                           <Grid item className={classes.grid}>
                              <TextField
                                 label="Number"
                                 style={{ width: 100 }}
                                 className={classes.textField}
                              />
                           </Grid>
                        </Grid>
                        <Grid container item direction="row" className={classes.fields}>
                           <Grid item className={classes.spacing}>
                              <Icon>mail</Icon>
                           </Grid>
                           <Grid item className={classes.spacing}>
                              <TextField
                                 label="Email"
                                 fullWidth={true}
                              />
                           </Grid>
                        </Grid>
                        <Grid container item direction="row" className={classes.fields}>
                           <Grid item className={classes.spacing}>
                              <Icon>location_on</Icon>
                           </Grid>
                           <Grid item className={classes.spacing}>
                              <TextField
                                 id="multiline-flexible"
                                 label="Address"
                                 multiline
                                 rowsMax="4"
                                 fullWidth={true}
                              />
                           </Grid>
                        </Grid>
                     </Grid>
                  </form>
            </Grid>
         </Grid >
         </div>

      );
   }
}
export default EditContact;