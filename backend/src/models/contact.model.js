// contact-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
const users = require('./users.model');
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');

  const Schema = mongooseClient.Schema;
  const contacts = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'users' },
    name: { type: String, required: true },
    phone: { type: Number }
  });

  return mongooseClient.model('contacts', contacts);
};