const Parser = require('../services/googleContactsLib/ContactParser');

function getDomain(mail){
    const splitMail = mail.split('@'); 
    return splitMail[1];
}
module.exports = {    
    getGoogleContacts(context) {
        return async function (context) {
            const domain = getDomain(context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.fetchContacts(context.params.user._id);
            context.result.data = res;
            return context;
        }
    },

    addPhoto(context) {
        return async function (context) {
            console.log("cr--", context);
            //const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
           // const res = await parse.createContact(context.data.firstName, context.data.lastName, context.data.phone, context.data.groupId);
            context.result = "res"
            return context;
        }
    },

    updateGoogleContacts(context) {
        return async function (context) {
            const domain = getDomain(context.params.user.email, context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.updateContact(context.data.firstName, context.data.lastName, context.data.phone, context.id, context.data.eTag, context.data.groupId);
            context.result = "res"
            return context;
        }
    },

    deleteGoogleContacts(context) {
        return async function (context) {
            const splitId = context.id.split('-');
            const domain = getDomain(context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.deleteContact(splitId[0], splitId[splitId.length - 1]);
            context.result = "res"
            return context;
        }
    }
};
