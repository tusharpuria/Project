const Parser = require('../services/googleContactsLib/ContactParser');

function getDomain(mail){
    const splitMail = mail.split('@'); 
    return splitMail[1];
}
module.exports = {    
    getGoogleGroups(context) {
        return async function (context) {
            const domain = getDomain(context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.fetchGroups();
            context.result.data = res;
            return context;
        }
    },

    createGoogleGroups(context) {
        return async function (context) {
            const domain = getDomain(context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.createGroup(context.data);
            context.result = "res"
            return context;
        }
    },

    updateGoogleGroups(context) {
        return async function (context) {
            const domain = getDomain(context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.updateGroup(context.data);
            context.result = "res"
            return context;
        }
    },

    deleteGoogleGroups(context) {
        return async function (context) {
            const domain = getDomain(context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.deleteGroup(context.id, context.params.query.groupTag);
            context.result = "res"
            return context;
        }
    }
};
