const Parser = require('../services/googleContactsLib/ContactParser');

function getDomain(mail) {
    const splitMail = mail.split('@');
    return splitMail[1];
}
module.exports = {
    getGoogleContacts(context) {
        return async function (context) {
            const domain = getDomain(context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.fetchContacts(context.params.user._id);
            context.result.data = res;
            return context;
        }
    },

    createGoogleContacts(context) {
        return async function (context) {            
            const domain = getDomain(context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.createContact(context.data);
            context.result = res
            return context;
        }
    },
    addGooglePhoto(context) {
        return async function (context) {
            if(context.params.file){
                const domain = getDomain(context.params.user.email);
                const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
                if(context.method === 'create'){
                    const res = await parse.addPhoto(context.result, context.params.file.buffer, context.data.photoTag);
                }
                else if(context.method === 'patch'){
                    const res = await parse.addPhoto(context.id, context.params.file.buffer, context.data.photoTag);
                }
            }
            context.result = "res"
            return context;
        }
    },

    updateGoogleContacts(context) {
        return async function (context) {
            const domain = getDomain(context.params.user.email, context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.updateContact(context.data, context.id);
            context.result = "res"
            return context;
        }
    },

    deleteGoogleContacts(context) {
        return async function (context) {
            const domain = getDomain(context.params.user.email);
            const parse = new Parser(context.params.user.email, domain, context.params.user.accessToken);
            const res = await parse.deleteContact(context.id, context.params.query.eTag);
            context.result = "res"
            return context;
        }
    }
};