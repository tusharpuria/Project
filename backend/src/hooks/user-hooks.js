
module.exports = function (options = {}) {
  return function (hook) {
    if (options.updateByKeys && options.updateByKeys.length > 0) {
      const primaryKeyField = hook.app.service(options.service).id
      const query = {}
      for (const updateByKey of options.updateByKeys) {
        query[updateByKey] = hook.data.google.profile.emails[0].value
      }
      return hook.app.service(options.service)
        .find({ query }).then(page => {
          if (page.total === 0) {
            hook.data.email = hook.data.google.profile.emails[0].value;
            hook.data.accessToken = hook.data.google.accessToken;
            hook.data.refreshToken = hook.data.google.refreshToken;

          } else {
            hook.app.service(options.service).patch(page.data[0][primaryKeyField], hook.data.google)
            hook.result = page.data[0]
          }
          return Promise.resolve(hook)
        })
    } else {
      return Promise.resolve(hook)
    }
  }
}