const { authenticate } = require('@feathersjs/authentication').hooks;
const userHooks = require('../../hooks/user-hooks');
const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;

module.exports = {
  before: {
    all: [],
    find: [authenticate('jwt')],
    get: [authenticate('jwt')],
    create: [authenticate('jwt'), hashPassword(), userHooks({ service: 'users', updateByKeys: ['email'] })],
    update: [hashPassword()],
    patch: [hashPassword()],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
