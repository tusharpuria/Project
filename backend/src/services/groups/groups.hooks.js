const { authenticate } = require('@feathersjs/authentication').hooks;
const groupHooks = require('../../hooks/groups-hook');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [groupHooks.createGoogleGroups()],
    update: [],
    patch: [groupHooks.updateGoogleGroups()],
    remove: [groupHooks.deleteGoogleGroups()]
  },

  after: {
    all: [ groupHooks.getGoogleGroups()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
