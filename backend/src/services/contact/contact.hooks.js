const { authenticate } = require('@feathersjs/authentication').hooks;
const authHooks = require('feathers-authentication-hooks');
const contactHooks = require('../../hooks/contact-hook');
const contactError = require('../../hooks/contact-error');

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [
      authHooks.queryWithCurrentUser({ idField: '_id', as: 'userId' })
    ],
    get: [
      authHooks.queryWithCurrentUser({ idField: '_id', as: 'userId' })
    ],
    create: [hashPassword(),
    authHooks.associateCurrentUser({ idField: '_id', as: 'userId' }),
    contactHooks.createGoogleContacts(), contactHooks.addGooglePhoto()
    ],
    update: [hashPassword()],
    patch: [hashPassword(),
    contactHooks.updateGoogleContacts(), contactHooks.addGooglePhoto()],
    remove: [
      authHooks.associateCurrentUser({ idField: '_id', as: 'userId' }),
      contactHooks.deleteGoogleContacts()
    ]
  },

  after: {
    all: [],
    find: [contactHooks.getGoogleContacts()],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};