// Initializes the `contact` service on path `/contact`
const createService = require('feathers-mongoose');
const createModel = require('../../models/contact.model');
const hooks = require('./contact.hooks');
var multer = require('multer')

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };
  var upload = multer();
  // Initialize our service with any options it requires
  app.use('/contact', upload.single('image'),
    function (req, res, next) {
      req.feathers = Object.assign(req.feathers, { file: req.file });
      next();
    },
    createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('contact');

  service.hooks(hooks);
};
