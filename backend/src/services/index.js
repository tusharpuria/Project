const contact = require('./contact/contact.service.js');
const users = require('./users/users.service.js');
const groups = require('./groups/groups.service.js');
const photo = require('./photo/photo.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(contact);
  app.configure(users);
  app.configure(groups);
};
