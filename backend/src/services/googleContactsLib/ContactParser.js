const axios = require('axios');
const rest = require('@feathersjs/rest-client');
const feathers = require('@feathersjs/feathers');
const app = feathers();
const restClient = rest();
app.configure(restClient.axios(axios));

module.exports = class Parser {
    constructor(userMail, userDomain, accessToken) {
        this.data = [];
        this.mail = userMail;
        this.domain = userDomain;
        this.accessToken = accessToken;
        this.url = "https://www.google.com/m8/feeds/contacts/" + userDomain;
        this.photoUrl = "https://www.google.com/m8/feeds/photos/media/" + userDomain
        this.groupUrl = "https://www.google.com/m8/feeds/groups/" + userDomain;
    }
    convertToXML(contactData) {
        let detail = `<atom:entry xmlns:atom="http://www.w3.org/2005/Atom"
                    xmlns:gd="http://schemas.google.com/g/2005"
                    xmlns:gContact="http://schemas.google.com/contact/2008" >
                <atom:category scheme="http://schemas.google.com/g/2005#kind"
                    term="http://schemas.google.com/contact/2008#contact"/>                
                <gd:phoneNumber rel="http://schemas.google.com/g/2005#${contactData.phoneType}"
                    primary="true">${contactData.phone}</gd:phoneNumber>`;
        if(contactData.email){
            detail = `${detail}<gd:email rel="http://schemas.google.com/g/2005#work" address="${contactData.email}"/>`
        }
        detail = `${detail}<gd:structuredPostalAddress rel="http://schemas.google.com/g/2005#work" primary="true">`
        for (const field in contactData) {
            if (field === 'city' || field === 'region' ||
                field === 'postcode' || field === 'country' || field === 'street') 
            {
                detail = `${detail}<gd:${field}>${contactData[field]}</gd:${field}>`
            }
        }
        detail = `${detail}</gd:structuredPostalAddress>`
        if (contactData.lastName == null) {
            detail = `${detail}
            <title>${contactData.firstName}</title>
            <gd:name>
                <gd:givenName>${contactData.firstName}</gd:givenName>
                <gd:fullName>${contactData.firstName}</gd:fullName>                    
            </gd:name>`
        }
        else {
            detail = `${detail}
            <title>${contactData.firstName} ${contactData.lastName}</title>
            <gd:name>
                <gd:givenName>${contactData.firstName}</gd:givenName>
                <gd:familyName>${contactData.lastName}</gd:familyName>
                <gd:fullName>${contactData.firstName} ${contactData.lastName}</gd:fullName>                    
            </gd:name>`
        }
        if (contactData.groupId !== '1') {
            detail = `${detail}
            <gContact:groupMembershipInfo
            href='http://www.google.com/m8/feeds/groups/${this.domain}/base/${contactData.groupId}'/>`
        }
        return `${detail}
                </atom:entry>`;
    }
    contactGroupXML(name) {
        return `<entry xmlns="http://www.w3.org/2005/Atom"
                        xmlns:gd="http://schemas.google.com/g/2005">
                    <category scheme="http://schemas.google.com/g/2005#kind"
                            term="http://schemas.google.com/g/2008#group"/>
                    <title>${name}</title>
                </entry>`;
    }
    async fetchContacts(userId) {
        return axios.get(this.url + "/full?alt=json", {
            headers: {
                'Authorization': 'Bearer ' + this.accessToken,
                'GData-Version': '3.0',
            }
        }).then(res => {
            if (res.data.feed.entry) {
                this.data = res.data.feed.entry
                    .map(contact => {
                        const links = contact.link.filter(link => {
                            return link.rel == 'self' || link.rel == 'edit';
                        })
                        const photoLink = contact.link.filter(link => {
                            return link.type == 'image/*';
                        })
                        const splitSelfLink = links[0].href.split('/');
                        const obj = {
                            "userId": userId, "fullName": contact.gd$name.gd$fullName.$t,
                            "firstName": contact.gd$name.gd$givenName.$t, "phone": contact.gd$phoneNumber[0].$t,
                            contactId: splitSelfLink[splitSelfLink.length - 1], eTag: contact.gd$etag,
                            photoTag: photoLink[0].gd$etag, photoUrl: photoLink[0].href + '?access_token=' + this.accessToken
                        };
                        if (typeof contact.gd$structuredPostalAddress !== "undefined") {
                            Object.assign(obj, {
                                address: {
                                    street: contact.gd$structuredPostalAddress[0].gd$street.$t,
                                    postcode: contact.gd$structuredPostalAddress[0].gd$postcode.$t,
                                    city: contact.gd$structuredPostalAddress[0].gd$city.$t,
                                    region: contact.gd$structuredPostalAddress[0].gd$region.$t,
                                    country: contact.gd$structuredPostalAddress[0].gd$country.$t,
                                },
                                formattedAddress: contact.gd$structuredPostalAddress[0].gd$formattedAddress
                            })
                        }
                        return typeof contact.gd$name.gd$familyName === "undefined" ?
                            obj : Object.assign(obj, { "lastName": contact.gd$name.gd$familyName.$t })
                    });
                return Promise.resolve(this.data);
            }
            else {
                return this.data = [];
            }
        }).catch(error => {
            console.log("error----", error.response.status)
        })
    }
    async createContact(contactData) {
        const body = this.convertToXML(contactData);
        return axios.post(this.url + "/full?alt=json", body, {
            headers: {
                'Authorization': 'Bearer ' + this.accessToken,
                'GData-Version': '3.0',
                'Content-Type': "application/atom+xml"
            }
        }).then(res => {
            const links = res.data.entry.link.filter(link => {
                return link.rel == 'self' || link.rel == 'edit';
            })
            const splitSelfLink = links[0].href.split('/');
            return Promise.resolve(splitSelfLink[splitSelfLink.length - 1]);
        }).catch(error => {
            console.log("error----", error)
        })
    }
    addPhoto(contactId, body, photoTag) {
        let headers = {
            'Authorization': 'Bearer ' + this.accessToken,
            'GData-Version': '3.0',
            'Content-Type': 'image/*'
        }
        if (photoTag) {
            headers = Object.assign(headers, { 'If-Match': photoTag })
        }
        return axios.put(this.photoUrl + `/${contactId}`, body, {
            headers
        }).then(res => {
            return res;
        }).catch((error) => {
            console.log("error----", error.response.status)
        })
    }
    updateContact(contactData, contactId) {
        const body = this.convertToXML(contactData);
        return axios.put(this.url + '/full/' + contactId, body, {
            headers: {
                'Authorization': 'Bearer ' + this.accessToken,
                'If-Match': contactData.eTag,
                'GData-Version': '3.0',
                'Content-Type': "application/atom+xml"
            }
        }).then(res => {
        }).catch(error => {
            console.log("error----", error.response.status)
        })
    }
    deleteContact(contactId, eTag) {
        return axios.delete(this.url + '/full/' + contactId, {
            headers: {
                'Authorization': 'Bearer ' + this.accessToken,
                'If-Match': eTag,
                'GData-Version': '3.0'
            }
        }).then(res => {
        }).catch(error => {
            console.log("error----", error.response.status)
        })
    }
    async fetchGroups() {
        return axios.get(this.groupUrl + "/full?alt=json", {
            headers: {
                'Authorization': 'Bearer ' + this.accessToken,
                'GData-Version': '3.0',
            }
        }).then(res => {
            if (res.data.feed.entry) {
                this.data = res.data.feed.entry.filter(group => !group.gContact$systemGroup)
                    .map(contact => {
                        const links = contact.link.filter(link => {
                            return link.rel == 'self' || link.rel == 'edit';
                        })
                        const splitSelfLink = links[0].href.split('/');
                        return {
                            "name": contact.title.$t, groupId: splitSelfLink[splitSelfLink.length - 1],
                            groupTag: contact.gd$etag
                        }
                    });
                return Promise.resolve(this.data);
            }
            else {
                return this.data = [];
            }
        }).catch(error => {
            console.log("error----", error.response.status)
        })
    }
    createGroup(groupData) {
        const body = this.contactGroupXML(groupData.name);
        return axios.post(this.groupUrl + "/full/", body, {
            headers: {
                'Authorization': 'Bearer ' + this.accessToken,
                'GData-Version': '3.0',
                'Content-Type': "application/atom+xml"
            }
        }).then(res => {
        }).catch(error => {
            console.log("error----", error.response.status)
        })
    }
    deleteGroup(groupId, groupTag) {
        return axios.delete(this.groupUrl + '/full/' + groupId, {
            headers: {
                'Authorization': 'Bearer ' + this.accessToken,
                'If-Match': groupTag,
                'GData-Version': '3.0'
            }
        }).then(res => {
        }).catch(error => {
            console.log("error----", error.response.status)
        })
    }
    updateGroup(groupData) {
        const body = this.contactGroupXML(groupData.name);
        return axios.put(this.groupUrl + '/full/' + groupData.groupId, body, {
            headers: {
                'Authorization': 'Bearer ' + this.accessToken,
                'If-Match': groupData.groupTag,
                'GData-Version': '3.0',
                'Content-Type': "application/atom+xml"
            }
        }).then(res => {
        }).catch(error => {
            console.log("error----", error.response.status)
        })
    }
}