// Initializes the `photo` service on path `/photo`
const createService = require('feathers-mongoose');
const createModel = require('../../models/photo.model');
const hooks = require('./photo.hooks');
const blobService = require('feathers-blob');
// Here we initialize a FileSystem storage,
// but you can use feathers-blob with any other
// storage service like AWS or Google Drive.
const fs = require('fs-blob-store');
const blobStorage = fs('./uploads');
const multer = require('multer');
const multipartMiddleware = multer();

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };
    
  // Initialize our service with any options it requires
  app.use('/photo', multipartMiddleware.single('uri'),

  // another middleware, this time to
  // transfer the received file to feathers
  function(req,res,next){
      req.feathers.file = req.file;
      next();
  }, blobService({ Model: blobStorage}));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('photo');

  service.hooks(hooks);
};
